// GLEW and GLFW
#include <GL/glew.h>
#include <GLFW/glfw3.h>

// STL
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

// imgui
#include "imgui_1.60/imgui.h"
#include "imgui_1.60/imgui_impl_glfw_gl3.h"

// OpenGL abstraction
#include "Renderer.h"
#include "VertexBuffer.h"
#include "VertexBufferLayout.h"
#include "IndexBuffer.h"
#include "VertexArray.h"
#include "Shader.h"
#include "Texture.h"

// GLM
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

// TESTS
#include "tests/Test.h"
#include "tests/Test_ClearColor.h"
#include "tests/Test_Texture2D.h"
#include "tests/Test_GLSL.h"

int main(void)
{
	GLFWwindow* window;

	/* Initialize the library */
	if (!glfwInit())
		return -1;

	// MSAA 4X
	// MSAA_init();

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	/* Create a windowed mode window and its OpenGL context */
	window = glfwCreateWindow(960, 540, "Hello OpenGL", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		return -1;
	}

	/* Make the window's context current */
	glfwMakeContextCurrent(window);

	// Limit frame rate to screen refresh rate
	glfwSwapInterval(1);
	std::cout << "INFO: Framerate is now locked to refresh rate!" << std::endl;

	if (glewInit() != GLEW_OK)
		std::cout << "Error!" << std::endl;

	// Check openGL version
	std::cout << glGetString(GL_VERSION) << std::endl;

	// Enable MSAA
	// MSAA_enable();

	{
		// Blending for transparency 
		GLCall(glEnable(GL_BLEND));
		GLCall(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));
		
		// Create a Renderer instance
		Renderer renderer_1;

		// imgui context
		ImGui::CreateContext();
		ImGui_ImplGlfwGL3_Init(window, true);
		ImGui::StyleColorsDark();

		// Test menu and tests creation
		test::Test* current_test = nullptr;
		test::TestMenu* test_menu = new test::TestMenu(current_test);
		current_test = test_menu;

		test_menu->RegisterTest<test::TestClearColor>("Clear Color");
		test_menu->RegisterTest<test::TestTexture>("Texture");
		test_menu->RegisterTest<test::TestGLSL>("GLSL Shader");
		
		const float bg_color = 0.63f;
		/* Loop until the user closes the window */
		while (!glfwWindowShouldClose(window))
		{
			/* Render here */
			renderer_1.Clear(bg_color, bg_color, bg_color, 1.0f);

			ImGui_ImplGlfwGL3_NewFrame();

			if (current_test)
			{
				current_test->OnUpdate(0.0f);
				current_test->OnRender();
				ImGui::Begin("Debug");
				if (current_test != test_menu && ImGui::Button("<-"))
				{
					delete current_test;
					current_test = test_menu;
				}
				current_test->OnImGUIRender();
				ImGui::End();
			}

			// imgui render
			ImGui::Render();
			ImGui_ImplGlfwGL3_RenderDrawData(ImGui::GetDrawData());

			/* Swap front and back buffers */
			glfwSwapBuffers(window);

			/* Poll for and process events */
			glfwPollEvents();
		}

		delete current_test;
		if (current_test != test_menu)
			delete test_menu;
	}

	ImGui_ImplGlfwGL3_Shutdown();
	ImGui::DestroyContext();
	glfwDestroyWindow(window);
	glfwTerminate();

	return 0;
}