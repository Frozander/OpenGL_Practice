#pragma once

#include <GL/glew.h>

#include "intern_libs/error_handling.h"
#include "intern_libs/MSAA.h"

#include "VertexArray.h"
#include "IndexBuffer.h"
#include "Shader.h"

class Renderer
{
public:
	Renderer();
	~Renderer();

	void Clear(const GLfloat r = 0.0f, const GLfloat g = 0.0f, const GLfloat b = 0.0f, const GLfloat a = 1.0f) const;
	void Draw(const VertexArray& va, const IndexBuffer& ib, const Shader& shader) const;

private:

};
