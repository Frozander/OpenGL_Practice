#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <unordered_map>

#include "glm/glm.hpp"
#include "intern_libs/error_handling.h"

struct ShaderProgramSource
{
	std::string VertexSource;
	std::string FragmentSource;
};

class Shader
{
public:
	Shader(const std::string& filepath);
	~Shader();

	void Bind() const;
	void Unbind() const;

	// Set Uniforms
	
	void SetUniform1i(const std::string& name, int v0);
	void SetUniform1f(const std::string& name, float value);
	void SetUniform2f(const std::string& name, glm::vec2& value);
	void SetUniform3f(const std::string& name, glm::vec3& value);
	void SetUniform4f(const std::string& name, glm::vec4& value);
	void SetUniformMat4f(const std::string& name, const glm::mat4 matrix);

private:
	unsigned int m_RendererID;
	std::string m_FilePath;

	int GetUniformLocation(const std::string& name);

	ShaderProgramSource ParseShader(const std::string& filepath);
	unsigned int CreateShader(const std::string& vertexShader, const std::string& fragmentShader);
	unsigned int CompileShader(unsigned int type, const std::string& source);

	//caching system
	std::unordered_map<std::string, GLint> m_UniformLocationCache;
};

