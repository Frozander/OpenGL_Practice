#include "MSAA.h"

bool MSAA_initialized = false;
bool MSAA_enabled = false;

void MSAA_init()
{
	glfwWindowHint(GLFW_SAMPLES, 4);
	MSAA_initialized = true;
}

void MSAA_enable()
{
	if (MSAA_initialized && !MSAA_enabled)
	{
		glEnable(GL_MULTISAMPLE);
		MSAA_enabled = true;
	}
	else std::cout << "MSAA is not initialised/disabled!" << std::endl;
}

void MSAA_disable()
{
	if (MSAA_initialized && MSAA_enabled)
	{
		glDisable(GL_MULTISAMPLE);
		MSAA_enabled = false;
	}
	else std::cout << "MSAA is not initialised/enabled!" << std::endl;

}