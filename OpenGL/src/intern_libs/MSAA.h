#pragma once
#ifndef __USR_MSAA
#define __USR_MSAA

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <iostream>

extern bool MSAA_initialized;
extern bool MSAA_enabled;

void MSAA_init();
void MSAA_enable();
void MSAA_disable();

#endif // !__USR_MSAA
