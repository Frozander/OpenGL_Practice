#include "error_handling.h"

//Deprecated
void __dep_GLClearError()
{
	while (glGetError() != GL_NO_ERROR);
}

//Deprecated
void __dep_GLCheckError()
{
	while (GLenum error = glGetError())
	{
		std::cout << "[OpenGL Error]" << "- 0x" << std::hex << error << std::endl;
	}
}

bool GLLogCall(const char* function, const char* file, int line)
{
	while (GLenum error = glGetError())
	{
		std::cout << "[OpenGL Error]" << " - 0x" << std::hex <<error << " in: "
			<< function << " in " << file << " line" << '[' << line << ']' << std::endl;
		return false;
	}
	return true;
}