#pragma once

#ifndef __ERR_HANDLING

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <iostream>

#define ASSERT(x) if (!(x)) __debugbreak();
#define GLCall(x) __dep_GLClearError();\
	x;\
	ASSERT(GLLogCall(#x, __FILE__, __LINE__))

//Deprecated
void __dep_GLClearError();
//Deprecated
void __dep_GLCheckError();
bool GLLogCall(const char* function, const char* file, int line);
#endif // !__ERR_HANDLING

