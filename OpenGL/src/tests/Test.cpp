#include "Test.h"
#include "imgui_1.60/imgui.h"

namespace test {

	TestMenu::TestMenu(Test*& current_test_pointer)
		: m_CurrentTest(current_test_pointer)
	{

	}

	void TestMenu::OnImGUIRender()
	{
		for (auto& test : m_Tests)
		{
			if (ImGui::Button(test.first.c_str())) {
				m_CurrentTest = test.second();
			}
		}
	}
}