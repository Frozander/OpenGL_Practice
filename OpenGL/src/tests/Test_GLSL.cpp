#include "Test_GLSL.h"

namespace test {

	TestGLSL::TestGLSL()
		:r_val(0.0f), r_increment(0.025f)
	{
		// 2d Vertex for Triangle
		float positions[] = {
			-0.5f, -0.5f,
			 0.5f, -0.5f,
			 0.5f,  0.5f,
			-0.5f,  0.5f
		};

		// index buffer
		unsigned int indices[] = {
			0, 1, 2,
			2, 3, 0
		};

		// Create vertex buffer, bind it and fill it with vertices from "positions" array
		m_VA = std::make_unique<VertexArray>();
		VertexBuffer vb(positions, 4 * 2 * sizeof(float));

		VertexBufferLayout layout;
		layout.Push<float>(2);
		m_VA->AddBuffer(vb, layout);

		// Create index buffer, bind it and fill it with vertices from "indices" array
		m_IB = std::make_unique<IndexBuffer>(indices, 6);

		m_Shader = std::make_unique<Shader>("res/shaders/GLSL_Test.shader");
		m_Shader->Bind();

		vals = glm::vec4(r_val, 0.3f, 0.8f, 1.0f);
		m_Shader->SetUniform4f("u_Color", vals);

		m_VA->Unbind();
		m_IB->Unbind();
		m_Shader->Unbind();
		vb.Unbind();

		m_Renderer = std::make_unique<Renderer>();
	}

	void TestGLSL::OnUpdate(float delta_time)
	{
		if (r_val > 1.0f || r_val < 0.0f) r_increment *= -1;
		r_val += r_increment;
		vals.r = r_val;
	}

	void TestGLSL::OnRender()
	{
		m_Renderer->Clear();

		m_Shader->Bind();
		m_Shader->SetUniform4f("u_Color", vals);

		m_Renderer->Draw(*m_VA, *m_IB, *m_Shader);
	}

}
