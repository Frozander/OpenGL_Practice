#pragma once

#include <memory>

#include "Test.h"
#include "Renderer.h"
#include "glm/glm.hpp"

namespace test {

	class TestGLSL : public Test
	{
	public:
		TestGLSL();

		void OnUpdate(float delta_time) override;
		void OnRender() override;
	private:
		float r_increment;
		float r_val;

		glm::vec4 vals;

		std::unique_ptr<Renderer> m_Renderer;
		std::unique_ptr<Shader> m_Shader;
		std::unique_ptr<VertexArray> m_VA;
		std::unique_ptr<IndexBuffer> m_IB;
	};
}