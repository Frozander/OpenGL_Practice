#include "Test_Texture2D.h"

namespace test {

	TestTexture::TestTexture()
	{
		// Scaling value
		float s = 2.0f;
		// 2d Vertex for Triangle	
		float positions[] = {
			 -50.0f * s,   -50.0f * s, 0.0f, 0.0f, // 0	
			  50.0f * s,   -50.0f * s, 1.0f, 0.0f, // 1	
			  50.0f * s,    50.0f * s, 1.0f, 1.0f, //2	
			 -50.0f * s,    50.0f * s, 0.0f, 1.0f //3	
		};

		// index buffer	
		unsigned int indices[] = {
			0, 1, 2,
			2, 3, 0
		};

		// translation matrices
		translation_1 = glm::vec3(200, 200, 0);
		translation_2 = glm::vec3(500, 200, 0);

		// Create vertex buffer, bind it and fill it with vertices from "positions" array	
		m_VA = std::make_unique<VertexArray>();
		VertexBuffer m_VB(positions, 4 * 4 * sizeof(float));

		VertexBufferLayout m_VBL;
		m_VBL.Push<float>(2);
		m_VBL.Push<float>(2);
		m_VA->AddBuffer(m_VB, m_VBL);

		// Create index buffer, bind it and fill it with vertices from "indices" array	
		m_IB = std::make_unique<IndexBuffer>(indices, 6);

		// Projection-View-Model matrix	
		proj = glm::ortho(0.0f, 960.0f, 0.0f, 540.0f, -1.0f, 1.0f);
		view = glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, 0));

		// Shader object creation and binding	
		m_Shader = std::make_unique<Shader>("res/shaders/basic.shader");
		m_Shader->Bind();

		// Set uniforms (initial setup)	
		//shader.SetUniform4f("u_Color", 0.8f, 0.3f, 0.8f, 1.0f);	

		// Create a texture object and fill it with given path, then bind it
		m_Texture = std::make_unique<Texture>("res/textures/tiktok.png");
		m_Texture->Bind(0);

		// Set shader uniform as a texture	
		m_Shader->SetUniform1i("u_Texture", 0);

		// Unbind everything	
		m_VA->Unbind();
		m_VB.Unbind();
		m_IB->Unbind();
		m_Shader->Unbind();
		//m_Texture->Unbind(); // Check if gives errors

		// Create renderer
		m_Renderer = std::make_unique<Renderer>();
	}

	void TestTexture::OnRender()
	{
		GLCall(glClearColor(0.7f, 0.7f, 0.7f, 1.0f));
		GLCall(glClear(GL_COLOR_BUFFER_BIT));

		{
			// Projection for shader 1	
			glm::mat4 model = glm::translate(glm::mat4(1.0f), translation_1);
			glm::mat4 mvp = proj * view * model;
			m_Shader->Bind();
			m_Shader->SetUniformMat4f("u_MVP", mvp);
			m_Renderer->Draw(*m_VA, *m_IB, *m_Shader);
		}

		// Second draw call	
		{
			// Projection for shader 2	
			glm::mat4 model = glm::translate(glm::mat4(1.0f), translation_2);
			glm::mat4 mvp = proj * view * model;
			m_Shader->Bind();
			m_Shader->SetUniformMat4f("u_MVP", mvp);
			m_Renderer->Draw(*m_VA, *m_IB, *m_Shader);
		}
	}

	void TestTexture::OnImGUIRender()
	{
		{
			ImGui::SliderFloat3("Translation_1", &translation_1.x, 0.0f, 1000.0f, "%.2f", 1.0f);
			ImGui::SliderFloat3("Translation_2", &translation_2.x, 0.0f, 1000.0f, "%.2f", 1.0f);
			ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
		}
	}
}