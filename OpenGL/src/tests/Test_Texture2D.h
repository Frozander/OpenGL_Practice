#pragma once

#include <memory>

#include "Test.h"
#include "Texture.h"

#include "Renderer.h"
#include "imgui_1.60/imgui.h"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
namespace test {

	class TestTexture : public Test 
	{
	public:
		TestTexture();

		void OnRender() override;
		void OnImGUIRender() override;
	private:

		float s;
		glm::vec3 translation_1, translation_2;
		glm::mat4 proj, model, view;

		std::unique_ptr<Renderer> m_Renderer;
		std::unique_ptr<Shader> m_Shader;
		std::unique_ptr<VertexArray> m_VA;
		std::unique_ptr<IndexBuffer> m_IB;
		std::unique_ptr<Texture> m_Texture;

	};
}