# OpenGL_Practice

This is an OpenGL repo I created following the tutorials from the youtube channel **The Cherno** (https://www.youtube.com/channel/UCQ-W1KE9EYfdxhL6S4twUNw)

So I don't claim I wrote any of this code since this was kind of a learning experience for me.

Note: After the commit 51fc869eac97ff94ed1d726b9df2dc06b713a43d the openGL series ends. So any possible addition is done by me. Adding this so if any errors occur I am one to blame

This repo includes:
* Immediate mode with old OpenGL
* Creating Vertex and Index buffers
* Creating and parsing single file shaders
* OpenGL abstractions in classes
* Texture inputs to shaders
* imGui windows for controlling uniforms
* Test frameworks
